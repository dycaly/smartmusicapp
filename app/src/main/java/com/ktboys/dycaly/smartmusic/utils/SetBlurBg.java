package com.ktboys.dycaly.smartmusic.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;



/**
 * Created by dycaly on 2016/11/15.
 */

public class SetBlurBg {

    public static void set(Activity activity, Bitmap bitmap, ViewGroup view){
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenWidth = dm.widthPixels;
        int screenHeight = dm.heightPixels;
        Log.i("屏幕宽高","Width:"+screenWidth+",Height:"+screenHeight);
        Bitmap aimBitmap = BlurBitmap.blur(activity,bitmap,screenWidth,screenHeight);
        view.setBackground(new BitmapDrawable(aimBitmap));
    }
}
