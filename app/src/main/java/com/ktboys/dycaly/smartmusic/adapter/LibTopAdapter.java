package com.ktboys.dycaly.smartmusic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ktboys.dycaly.smartmusic.R;
import com.ktboys.dycaly.smartmusic.dataitem.LibTopItem;
import com.ktboys.dycaly.smartmusic.dataitem.SocialItem;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dycaly on 2016/11/18.
 */

public class LibTopAdapter extends BaseAdapter {
    private ArrayList<LibTopItem> datas;
    private Context context;
    public LibTopAdapter(Context context){
        this.context = context;
        datas = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return datas.size();
    }
    public void clear(){
        datas.clear();
        notifyDataSetChanged();
    }
    public void addItem(LibTopItem item){
        datas.add(item);
        notifyDataSetChanged();
    }
    @Override
    public LibTopItem getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder = null;
        if (view == null){
            holder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.lv_library_item,null);
            holder.image = (ImageView) view.findViewById(R.id.id_lli_image);
            holder.titleText = (TextView) view.findViewById(R.id.id_lli_title);
            holder.numText = (TextView) view.findViewById(R.id.id_lli_num);
            view.setTag(holder);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }
        LibTopItem item = datas.get(i);
        holder.image.setImageResource(item.getImage());
        holder.titleText.setText(item.getTitle());
        holder.numText.setText(item.getNumber()+"");
        return view;
    }

    class ViewHolder{
        ImageView image;
        TextView titleText;
        TextView numText;
    }
}
