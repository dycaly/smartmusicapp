package com.ktboys.dycaly.smartmusic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ktboys.dycaly.smartmusic.R;
import com.ktboys.dycaly.smartmusic.dataitem.MenuItem;
import com.ktboys.dycaly.smartmusic.dataitem.SocialItem;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dycaly on 2016/11/18.
 */

public class SocialAdapter extends BaseAdapter {
    private ArrayList<SocialItem> datas;
    private Context context;
    public SocialAdapter(Context context){
        this.context = context;
        datas = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return datas.size();
    }
    public void clear(){
        datas.clear();
        notifyDataSetChanged();
    }
    public void addItem(SocialItem item){
        datas.add(item);
        notifyDataSetChanged();
    }
    @Override
    public SocialItem getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder = null;
        if (view == null){
            holder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.lv_social_item,null);
            holder.headImage = (CircleImageView) view.findViewById(R.id.id_lsi_head);
            holder.nameText = (TextView) view.findViewById(R.id.id_lsi_name);
            holder.sexImage = (ImageView) view.findViewById(R.id.id_lsi_sex);
            holder.distanceText = (TextView) view.findViewById(R.id.id_lsi_distance);
            holder.moodImage = (ImageView) view.findViewById(R.id.id_lsi_mood);
            holder.musicText = (TextView) view.findViewById(R.id.id_lsi_music);
            view.setTag(holder);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }
        SocialItem item = datas.get(i);
        holder.headImage.setImageResource(item.getHeadImage());
        holder.nameText.setText(item.getName());
        holder.sexImage.setImageResource(item.getSex()==0?R.drawable.ic_male:R.drawable.ic_female);
        holder.distanceText.setText(item.getDistance());
        holder.moodImage.setImageResource(item.getMoodImage());
        holder.musicText.setText(item.getMusic());
        return view;
    }

    class ViewHolder{
        CircleImageView headImage;
        TextView nameText;
        ImageView sexImage;
        TextView distanceText;
        ImageView moodImage;
        TextView musicText;
    }
}
