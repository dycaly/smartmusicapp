package com.ktboys.dycaly.smartmusic.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ktboys.dycaly.smartmusic.R;

/**
 * Created by dycaly on 2016/11/18.
 */

public class SettingFragment extends Fragment {
    private View rootView;
    private LinearLayout quitLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_setting, container, false);
        initView();
        initEvent();
        return rootView;
    }

    private void initView() {
        quitLayout = (LinearLayout) rootView.findViewById(R.id.id_setting_quit);
    }

    private void initEvent() {
        quitLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
    }


}
