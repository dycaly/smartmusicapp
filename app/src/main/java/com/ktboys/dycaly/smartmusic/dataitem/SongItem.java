package com.ktboys.dycaly.smartmusic.dataitem;

/**
 * Created by dycaly on 2016/11/19.
 */

public class SongItem {
    private int id;
    private String filePath;
    private String title;
    private String album;
    private String artist;
    private String mood;
    private boolean like;
    private boolean skip;

    public SongItem() {
    }

    public SongItem(int id, String filePath, String title, String album, String artist, String mood, boolean like, boolean skip) {
        this.id = id;
        this.filePath = filePath;
        this.title = title;
        this.album = album;
        this.artist = artist;
        this.mood = mood;
        this.like = like;
        this.skip = skip;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }
}
