package com.ktboys.dycaly.smartmusic.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.ktboys.dycaly.smartmusic.R;

/**
 * Created by dycaly on 2016/11/18.
 */

public class SearchFragment extends Fragment {
    private View rootView;
    private EditText searchEdit;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search, container, false);
        searchEdit = (EditText) rootView.findViewById(R.id.id_search_et);
        searchEdit.clearFocus();
        return rootView;
    }
    public void close(){
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchEdit.getWindowToken(),0);
    }
}
