package com.ktboys.dycaly.smartmusic.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ktboys.dycaly.smartmusic.R;

/**
 * Created by dycaly on 2016/11/18.
 */

public class BrowseFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i("Fragment","初始化+++++++++++++++++++++++++++++++++");
        return inflater.inflate(R.layout.fragment_browse, container, false);
    }
}
