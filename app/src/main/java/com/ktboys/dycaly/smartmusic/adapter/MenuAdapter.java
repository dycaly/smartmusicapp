package com.ktboys.dycaly.smartmusic.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ktboys.dycaly.smartmusic.R;
import com.ktboys.dycaly.smartmusic.dataitem.MenuItem;

import java.util.ArrayList;

/**
 * Created by dycaly on 2016/11/18.
 */

public class MenuAdapter extends BaseAdapter {
    private ArrayList<MenuItem> datas;
    private Context context;
    public MenuAdapter(Context context){
        this.context = context;
        datas = new ArrayList<>();
    }
    public void selectItem(int index){
        for(MenuItem item :datas){
            item.setSelected(false);
        }
        datas.get(index).setSelected(true);
        notifyDataSetChanged();
    }
    public void selectNothing(){
        for(MenuItem item :datas){
            item.setSelected(false);
        }
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return datas.size();
    }
    public void addItem(MenuItem item){
        datas.add(item);
        notifyDataSetChanged();
    }
    @Override
    public MenuItem getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder = null;
        if (view == null){
            holder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.lv_menu_item,null);
            holder.leftLayout = (LinearLayout) view.findViewById(R.id.id_left);
            holder.titleText = (TextView) view.findViewById(R.id.id_title);
            view.setTag(holder);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }
        MenuItem item = datas.get(i);
        holder.titleText.setText(item.getTitle());
        holder.titleText.setTextColor(item.isSelected()?0xFFE78500:0xFFFFFFFF);
        holder.leftLayout.setBackgroundResource(item.isSelected()?R.drawable.ic_menu_left:R.color.colorTransparent);
        return view;
    }

    class ViewHolder{
        LinearLayout leftLayout;
        TextView titleText;
    }
}
