package com.ktboys.dycaly.smartmusic.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ktboys.dycaly.smartmusic.dataitem.SongItem;
import com.ktboys.dycaly.smartmusic.utils.DataBaseHelper;

import java.util.ArrayList;


/**
 * Created by dycaly on 2015/12/18.
 */
public class SongsManager {

    private DataBaseHelper mDataBaseHelper;
    private SQLiteDatabase mDb;

    public SongsManager(Context context) {
        mDataBaseHelper = DataBaseHelper.getInstance(context);
        mDb = mDataBaseHelper.getWritableDatabase();
    }

    public boolean addSong(SongItem item) {

        try {
            Log.i("数据库提示", "添加歌曲信息事务-----开始");
            mDb.beginTransaction();
            ContentValues values = new ContentValues();
            values.put("filepath", item.getFilePath());
            values.put("title", item.getTitle());
            values.put("album", item.getAlbum());
            values.put("artist", item.getArtist());
            values.put("mood", item.getMood());
            values.put("like", item.isLike());
            values.put("skip", item.isSkip());
            mDb.insert("song", null, values);
            mDb.setTransactionSuccessful();
        } catch (Exception e) {
            Log.i("数据库提示", "添加歌曲信息事务错误:" + e.getMessage());
            return false;
        } finally {
            mDb.endTransaction();
            Log.i("数据库提示", "添加歌曲信息事务-----成功");
            return true;
        }
    }

    public SongItem getFirstSong(String mood) {
        Log.i("心情", mood);
        try {
            Cursor cursor = mDb.query("song", null, "mood=?", new String[]{mood}, null, null, null, null);
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex("_id"));
                String filepath = cursor.getString(cursor.getColumnIndex("filepath"));
                String title = cursor.getString(cursor.getColumnIndex("title"));
                String album = cursor.getString(cursor.getColumnIndex("album"));
                String artist = cursor.getString(cursor.getColumnIndex("artist"));
                int like = cursor.getInt(cursor.getColumnIndex("like"));
                int skip = cursor.getInt(cursor.getColumnIndex("skip"));
                return new SongItem(id, filepath, title, album, artist, mood, like == 1, skip == 1);
            }
            return null;
        } catch (Exception e) {
            Log.i("获取第一首歌失败", e.getMessage());
            return null;
        }

    }

    public SongItem getNextSong(String mood, int curId) {
        try {
            Cursor cursor = mDb.query("song", null, "mood=?", new String[]{mood}, null, null, null, null);
            SongItem firstItem = null;
            boolean isReturen = false;
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex("_id"));
                String filepath = cursor.getString(cursor.getColumnIndex("filepath"));
                String title = cursor.getString(cursor.getColumnIndex("title"));
                String album = cursor.getString(cursor.getColumnIndex("album"));
                String artist = cursor.getString(cursor.getColumnIndex("artist"));
                int like = cursor.getInt(cursor.getColumnIndex("like"));
                int skip = cursor.getInt(cursor.getColumnIndex("skip"));
                if (firstItem == null) {
                    firstItem = new SongItem(id, filepath, title, album, artist, mood, like == 1, skip == 1);
                }
                if (isReturen) {
                    return new SongItem(id, filepath, title, album, artist, mood, like == 1, skip == 1);
                }
                if (id == curId) {
                    isReturen = true;
                }

            }
            return firstItem;
        } catch (Exception e) {
            return null;
        }
    }

    public SongItem getLastSong(String mood, int curId) {
        try {
            Cursor cursor = mDb.query("song", null, "mood=?", new String[]{mood}, null, null, null, null);
            SongItem lastItem = null;
            boolean isReturen = true;
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex("_id"));
                String filepath = cursor.getString(cursor.getColumnIndex("filepath"));
                String title = cursor.getString(cursor.getColumnIndex("title"));
                String album = cursor.getString(cursor.getColumnIndex("album"));
                String artist = cursor.getString(cursor.getColumnIndex("artist"));
                int like = cursor.getInt(cursor.getColumnIndex("like"));
                int skip = cursor.getInt(cursor.getColumnIndex("skip"));
                if (lastItem == null && id == curId) {
                    isReturen = false;
                }
                if (id == curId && isReturen) {
                    return lastItem;
                }
                lastItem = new SongItem(id, filepath, title, album, artist, mood, like == 1, skip == 1);

            }
            return lastItem;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean setLiske(int id, boolean like) {
        try{
            ContentValues values = new ContentValues();
            values.put("like", like);
            int rlt = mDb.update("song", values, "_id = ?", new String[]{"" + id});
            return (rlt == 0) ? false : true;
        }
        catch (Exception e){
            Log.i("修改Like","失败");
            return false;
        }
    }

    public void clearSongs() {
        mDb.delete("song", null, null);
    }

    public void close() {
        mDb.close();
    }
}

