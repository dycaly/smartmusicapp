package com.ktboys.dycaly.smartmusic.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ktboys.dycaly.smartmusic.R;
import com.ktboys.dycaly.smartmusic.adapter.SocialAdapter;
import com.ktboys.dycaly.smartmusic.dataitem.SocialItem;

/**
 * Created by dycaly on 2016/11/18.
 */

public class SocialFragment extends Fragment {

    private View rootView;
    private LinearLayout allLayout;
    private TextView allText;
    private LinearLayout allIndLayout;
    private LinearLayout foLayout;
    private TextView foText;
    private LinearLayout foIndLayout;

    private ListView mListView;
    private SocialAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_social, container, false);
        initView();
        initEvent();
        return rootView;
    }
    private void initView() {
        allLayout = (LinearLayout) rootView.findViewById(R.id.id_top_all_ll);
        allText = (TextView) rootView.findViewById(R.id.id_top_all_tv);
        allIndLayout = (LinearLayout) rootView.findViewById(R.id.id_top_all_ind);
        foLayout = (LinearLayout) rootView.findViewById(R.id.id_top_fo_ll);
        foText = (TextView) rootView.findViewById(R.id.id_top_fo_tv);
        foIndLayout = (LinearLayout) rootView.findViewById(R.id.id_top_fo_ind);
        mListView = (ListView) rootView.findViewById(R.id.id_browse_lv);
        mAdapter = new SocialAdapter(getActivity());
        mListView.setAdapter(mAdapter);
        mAdapter.addItem(new SocialItem(R.drawable.ic_mike,"Mike",0,"150m",R.drawable.ic_happy,"Now Playing:More Than Words-Extreme"));
        mAdapter.addItem(new SocialItem(R.drawable.ic_rachel,"Rachel",1,"500m",R.drawable.ic_mood_romantic,"Now Playing:Wonderful Tonight-Eric"));
        mAdapter.addItem(new SocialItem(R.drawable.ic_ross,"Ross",0,">1km",R.drawable.ic_mood_relaxing,"Now Playing:Money-Pink Floyd"));
    }
    private void initEvent() {
        allLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("点击","-----------------------");
                allText.setTextColor(0xFFFF9000);
                allIndLayout.setBackgroundColor(0xFFFF9000);
                foText.setTextColor(0xFFFFFFFF);
                foIndLayout.setBackgroundColor(0x00FFFFFF);
                mAdapter.clear();
                mAdapter.addItem(new SocialItem(R.drawable.ic_mike,"Mike",0,"150m",R.drawable.ic_happy,"Now Playing:More Than Words-Extreme"));
                mAdapter.addItem(new SocialItem(R.drawable.ic_rachel,"Rachel",1,"500m",R.drawable.ic_mood_romantic,"Now Playing:Wonderful Tonight-Eric"));
                mAdapter.addItem(new SocialItem(R.drawable.ic_ross,"Ross",0,">1km",R.drawable.ic_mood_relaxing,"Now Playing:Money-Pink Floyd"));
            }
        });
        foLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("点击","+++++++++++++++++++++++++++");
                allText.setTextColor(0xFFFFFFFF);
                allIndLayout.setBackgroundColor(0x00FFFFFF);
                foText.setTextColor(0xFFFF9000);
                foIndLayout.setBackgroundColor(0xFFFF9000);
                mAdapter.clear();
                mAdapter.addItem(new SocialItem(R.drawable.ic_rachel,"Rachel",1,"500m",R.drawable.ic_mood_romantic,"Now Playing:Wonderful Tonight-Eric"));
                mAdapter.addItem(new SocialItem(R.drawable.ic_ross,"Ross",0,">1km",R.drawable.ic_mood_relaxing,"Now Playing:Money-Pink Floyd"));
            }
        });
    }

}
