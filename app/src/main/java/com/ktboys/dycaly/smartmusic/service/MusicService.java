package com.ktboys.dycaly.smartmusic.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;

import com.ktboys.dycaly.smartmusic.activity.MainActivity;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by dycaly on 2016/11/16.
 */

public class MusicService extends Service {

    private MediaPlayer mMediaPlayer;
    private Timer mTimer;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MusicCtrl();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mMediaPlayer.start();
                addTimer();
            }
        });
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                delTimer();
                Message msg = MainActivity.compliteHandler.obtainMessage();
                MainActivity.compliteHandler.sendMessage(msg);
            }
        });
        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                delTimer();
                Message msg = MainActivity.errorHandler.obtainMessage();
                MainActivity.errorHandler.sendMessage(msg);
                return false;
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        delTimer();
        mMediaPlayer.stop();
        mMediaPlayer.release();
    }

    public void playMusic(String name) {
        Log.i("播放音乐", "开始播放");
        delTimer();
        mMediaPlayer.reset();
        try {
            mMediaPlayer.setDataSource(name);
            mMediaPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print(e.getMessage());
        }
    }

    public void pauseMusic() {
        mMediaPlayer.pause();
    }

    public void continueMusic() {
        mMediaPlayer.start();
    }
    public void stopMusic(){
        mMediaPlayer.stop();
        delTimer();
        sendProcess(0);
    }

    public void addTimer() {
        if (mTimer == null) {
            mTimer = new Timer();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    int duration = mMediaPlayer.getDuration();
                    int currentPosition = mMediaPlayer.getCurrentPosition();
                    sendProcess(duration == 0 ? 0 : 360 * (double) currentPosition / (double) duration);
                }
            }, 0, 100);
        }
    }
    public void delTimer(){
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    public void sendProcess(double process) {
        Message msg = MainActivity.processHandler.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putDouble("process", process);
        msg.setData(bundle);
        MainActivity.processHandler.sendMessage(msg);
    }

    class MusicCtrl extends Binder implements MusicControler {

        @Override
        public void playMusic(String name) {
            MusicService.this.playMusic(name);
        }

        @Override
        public void pauseMusic() {
            MusicService.this.pauseMusic();
        }

        @Override
        public void continueMusic() {
            MusicService.this.continueMusic();
        }

        @Override
        public void stopMusic() {
            MusicService.this.stopMusic();
        }
    }

    public interface MusicControler {
        void playMusic(String name);

        void pauseMusic();

        void continueMusic();

        void stopMusic();
    }
}

