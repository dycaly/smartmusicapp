package com.ktboys.dycaly.smartmusic.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ktboys.dycaly.smartmusic.R;
import com.ktboys.dycaly.smartmusic.dataitem.MoodItem;

import java.util.ArrayList;

/**
 * Created by dycaly on 2016/11/18.
 */

public class MoodAdapter extends BaseAdapter {

    private ArrayList<MoodItem> datas;
    private Context context;

    public MoodAdapter(Context context){
        this.context = context;
        datas = new ArrayList<>();
    }
    public void clear(){
        datas.clear();
        notifyDataSetChanged();
    }
    public void addItem(MoodItem item){
        datas.add(item);
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public MoodItem getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null){
            holder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.gv_mood_item,null);
            holder.moodImage= (ImageView) view.findViewById(R.id.id_mood_iv);
            holder.moodTitle = (TextView) view.findViewById(R.id.id_mood_tv);
            view.setTag(holder);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }
        MoodItem item = datas.get(i);
        holder.moodImage.setImageBitmap(BitmapFactory.decodeResource(context.getResources(),item.getMoodImage()));
        holder.moodTitle.setText(item.getTitle());
        return view;
    }

    class ViewHolder{
        public ImageView moodImage;
        public TextView moodTitle;
    }
}
