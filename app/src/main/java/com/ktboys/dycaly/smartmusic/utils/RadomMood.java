package com.ktboys.dycaly.smartmusic.utils;

import com.ktboys.dycaly.smartmusic.dataitem.SocialItem;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by dycaly on 2016/11/23.
 */

public class RadomMood {
    public static String getRadomMood() {
        ArrayList<String> moods = new ArrayList<>();
        moods.add("energetic");
        moods.add("romantic");
        moods.add("bright");
        moods.add("relaxing");
        moods.add("angry");
        moods.add("gloomy");
        Random random = new Random();
        return moods.get(random.nextInt(6));
    }
}
