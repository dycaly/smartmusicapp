package com.ktboys.dycaly.smartmusic.dataitem;

/**
 * Created by dycaly on 2016/11/22.
 */

public class LibTopItem {
    private int image;
    private String title;
    private int number;

    public LibTopItem(int image, String title, int number) {
        this.image = image;
        this.title = title;
        this.number = number;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
