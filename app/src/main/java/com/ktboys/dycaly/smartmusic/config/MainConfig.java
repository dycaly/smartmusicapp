package com.ktboys.dycaly.smartmusic.config;

import android.os.Environment;

/**
 * Created by dycaly on 2016/11/16.
 */

public class MainConfig {
    public static String MUSIC_PATH = Environment.getExternalStorageDirectory() + "/SmartMusic";
}
