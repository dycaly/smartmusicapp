package com.ktboys.dycaly.smartmusic.view;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ktboys.dycaly.smartmusic.R;

import java.util.Timer;
import java.util.TimerTask;


public class CircleImageView extends ImageView {

    private static final ScaleType SCALE_TYPE = ScaleType.CENTER_CROP;

    private static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.ARGB_8888;
    private static final int COLORDRAWABLE_DIMENSION = 2;

    private static final int DEFAULT_BORDER_WIDTH = 0;
    private static final int DEFAULT_BORDER_COLOR = Color.BLACK;
    private static final int DEFAULT_FILL_COLOR = Color.TRANSPARENT;
    private static final boolean DEFAULT_BORDER_OVERLAY = false;

    private final RectF mDrawableRect = new RectF();
    private final RectF mBorderRect = new RectF();

    private final Matrix mShaderMatrix = new Matrix();
    private final Paint mBitmapPaint = new Paint();
    private final Paint mBorderPaint = new Paint();
    private final Paint mFillPaint = new Paint();
    private final Paint mProcessPaint = new Paint();

    private int mBorderColor = DEFAULT_BORDER_COLOR;
    private int mBorderWidth = DEFAULT_BORDER_WIDTH;
    private int mFillColor = DEFAULT_FILL_COLOR;
    private int mProcessColor = DEFAULT_FILL_COLOR;

    private Bitmap mBitmap;
    private BitmapShader mBitmapShader;
    private int mBitmapWidth;
    private int mBitmapHeight;

    private float mDrawableRadius;
    private float mBorderRadius;

    private ColorFilter mColorFilter;

    private boolean mReady;
    private boolean mSetupPending;
    private boolean mBorderOverlay;
    private Context mContext;

    private double mProcess = 0;
    private RectF mProcessRect;

    private RectF mDotRect;
    private Bitmap dotBitmap;
    private Paint mDotPaint;

    public static int TOUCH_NORMAL = 1;
    public static int TOUCH_DOWN = 2;
    public static int TOUCH_MOVE = 3;
    public static int TOUCH_PROCESS = 4;

    private int state = TOUCH_NORMAL;

    private float lastX = 0;
    private float lastY = 0;
    private float slideX = 0;
    private float slideY = 0;
    private float distanceX = 0;
    private float distanceY = 0;
    private TouchSlideEvent mTouchSlideEvent;
    private int w = 0;
    private RelativeLayout.LayoutParams layoutParams;

    private int slideDirect = 0;

    public void setProcess(double process) {
        mProcess = process;
        invalidate();
    }

    public void setTouchSlideEvent(TouchSlideEvent touchSlideEvent) {
        this.mTouchSlideEvent = touchSlideEvent;
    }


    public CircleImageView(Context context) {
        super(context);

        init();
    }

    public CircleImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleImageView, defStyle, 0);

        mBorderWidth = a.getDimensionPixelSize(R.styleable.CircleImageView_civ_border_width, DEFAULT_BORDER_WIDTH);
        mBorderColor = a.getColor(R.styleable.CircleImageView_civ_border_color, DEFAULT_BORDER_COLOR);
        mBorderOverlay = a.getBoolean(R.styleable.CircleImageView_civ_border_overlay, DEFAULT_BORDER_OVERLAY);
        mFillColor = a.getColor(R.styleable.CircleImageView_civ_fill_color, DEFAULT_FILL_COLOR);
        mProcessColor = a.getColor(R.styleable.CircleImageView_civ_process_color, DEFAULT_FILL_COLOR);

        a.recycle();
        init();

    }

    private void init() {
        super.setScaleType(SCALE_TYPE);
        mReady = true;
        if (mSetupPending) {
            setup();
            mSetupPending = false;
        }
    }


    @Override
    public ScaleType getScaleType() {
        return SCALE_TYPE;
    }

    @Override
    public void setScaleType(ScaleType scaleType) {
        if (scaleType != SCALE_TYPE) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", scaleType));
        }
    }

    @Override
    public void setAdjustViewBounds(boolean adjustViewBounds) {
        if (adjustViewBounds) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mBitmap == null) {
            return;
        }

        if (mFillColor != Color.TRANSPARENT) {
            canvas.drawCircle(getWidth() / 2.0f, getHeight() / 2.0f, mDrawableRadius, mFillPaint);
        }

        canvas.drawCircle(getWidth() / 2.0f, getHeight() / 2.0f, mDrawableRadius, mBitmapPaint);
        if (mBorderWidth != 0) {
            canvas.drawCircle(getWidth() / 2.0f, getHeight() / 2.0f, mBorderRadius, mBorderPaint);
            canvas.drawArc(mProcessRect, -90, (float) mProcess, false, mProcessPaint);

            double r = (mProcessRect.right - mProcessRect.left) / 2;
            double x = r * Math.sin(Math.PI * (mProcess / 180.0)) + (mProcessRect.left + mProcessRect.right) / 2;
            double y = -r * Math.cos(Math.PI * (mProcess / 180.0)) + (mProcessRect.left + mProcessRect.right) / 2;
            dotBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_dot);
            mDotRect = new RectF((float) (x - mBorderWidth * 2), (float) (y - mBorderWidth * 2), (float) (x + mBorderWidth * 2), (float) (y + mBorderWidth * 2));

            canvas.drawBitmap(dotBitmap, null, mDotRect, mDotPaint);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        setup();
    }

    public int getBorderColor() {
        return mBorderColor;
    }

    public void setBorderColor(@ColorInt int borderColor) {
        if (borderColor == mBorderColor) {
            return;
        }

        mBorderColor = borderColor;
        mBorderPaint.setColor(mBorderColor);
        invalidate();
    }

    public void setBorderColorResource(@ColorRes int borderColorRes) {
        setBorderColor(getContext().getResources().getColor(borderColorRes));
    }

    public int getFillColor() {
        return mFillColor;
    }

    public void setFillColor(@ColorInt int fillColor) {
        if (fillColor == mFillColor) {
            return;
        }

        mFillColor = fillColor;
        mFillPaint.setColor(fillColor);
        invalidate();
    }

    public void setFillColorResource(@ColorRes int fillColorRes) {
        setFillColor(getContext().getResources().getColor(fillColorRes));
    }

    public int getBorderWidth() {
        return mBorderWidth;
    }

    public void setBorderWidth(int borderWidth) {
        if (borderWidth == mBorderWidth) {
            return;
        }

        mBorderWidth = borderWidth;
        setup();
    }

    public boolean isBorderOverlay() {
        return mBorderOverlay;
    }

    public void setBorderOverlay(boolean borderOverlay) {
        if (borderOverlay == mBorderOverlay) {
            return;
        }

        mBorderOverlay = borderOverlay;
        setup();
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        mBitmap = bm;
        setup();
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        mBitmap = getBitmapFromDrawable(drawable);
        setup();
    }

    @Override
    public void setImageResource(@DrawableRes int resId) {
        super.setImageResource(resId);
        mBitmap = getBitmapFromDrawable(getDrawable());
        setup();
    }

    @Override
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        mBitmap = uri != null ? getBitmapFromDrawable(getDrawable()) : null;
        setup();
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        if (cf == mColorFilter) {
            return;
        }

        mColorFilter = cf;
        mBitmapPaint.setColorFilter(mColorFilter);
        invalidate();
    }

    private Bitmap getBitmapFromDrawable(Drawable drawable) {
        if (drawable == null) {
            return null;
        }

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        try {
            Bitmap bitmap;

            if (drawable instanceof ColorDrawable) {
                bitmap = Bitmap.createBitmap(COLORDRAWABLE_DIMENSION, COLORDRAWABLE_DIMENSION, BITMAP_CONFIG);
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(),
                        BITMAP_CONFIG);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void setup() {

        if (!mReady) {
            mSetupPending = true;
            return;
        }

        if (getWidth() == 0 && getHeight() == 0) {
            return;
        }

        if (mBitmap == null) {
            invalidate();
            return;
        }

        mBitmapShader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        mBitmapPaint.setAntiAlias(true);
        mBitmapPaint.setShader(mBitmapShader);

        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setAntiAlias(true);
        mBorderPaint.setColor(mBorderColor);
        mBorderPaint.setStrokeWidth(mBorderWidth);

        mFillPaint.setStyle(Paint.Style.FILL);
        mFillPaint.setAntiAlias(true);
        mFillPaint.setColor(mFillColor);

        mProcessPaint.setStyle(Paint.Style.STROKE);
        mProcessPaint.setStrokeWidth(mBorderWidth);
        mProcessPaint.setAntiAlias(true);
        mProcessPaint.setColor(mProcessColor);

        mDotPaint = new Paint();
        mDotPaint.setDither(true);
        mDotPaint.setAntiAlias(true);

        mBitmapHeight = mBitmap.getHeight();
        mBitmapWidth = mBitmap.getWidth();

        mBorderRect.set(0 + mBorderWidth * 2, 0 + mBorderWidth * 2, getWidth() - mBorderWidth * 2, getHeight() - mBorderWidth * 2);
        mBorderRadius = Math.min((mBorderRect.height() - mBorderWidth) / 2.0f,
                (mBorderRect.width() - mBorderWidth) / 2.0f);

        mDrawableRect.set(mBorderRect);
        if (!mBorderOverlay) {
            mDrawableRect.inset(mBorderWidth, mBorderWidth);
        }
        mDrawableRadius = Math.min(mDrawableRect.height() / 2.0f, mDrawableRect.width() / 2.0f);
        float left = (mDrawableRect.left + mBorderRect.left) / 2;
        float top = (mDrawableRect.top + mBorderRect.top) / 2;
        float right = (mDrawableRect.right + mBorderRect.right) / 2;
        float bottom = (mDrawableRect.bottom + mBorderRect.bottom) / 2;
        mProcessRect = new RectF(left, top, right, bottom);
        updateShaderMatrix();
        if (layoutParams == null) {
            layoutParams = (RelativeLayout.LayoutParams) this.getLayoutParams();
        }
        invalidate();
    }

    private void updateShaderMatrix() {
        float scale;
        float dx = 0;
        float dy = 0;

        mShaderMatrix.set(null);

        if (mBitmapWidth * mDrawableRect.height() > mDrawableRect.width() * mBitmapHeight) {
            scale = mDrawableRect.height() / (float) mBitmapHeight;
            dx = (mDrawableRect.width() - mBitmapWidth * scale) * 0.5f;
        } else {
            scale = mDrawableRect.width() / (float) mBitmapWidth;
            dy = (mDrawableRect.height() - mBitmapHeight * scale) * 0.5f;
        }

        mShaderMatrix.setScale(scale, scale);
        mShaderMatrix.postTranslate((int) (dx + 0.5f) + mDrawableRect.left, (int) (dy + 0.5f) + mDrawableRect.top);

        mBitmapShader.setLocalMatrix(mShaderMatrix);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastX = event.getX();
                lastY = event.getY();
                slideX = event.getRawX();
                slideY = event.getRawY();
                if (lastX > mDotRect.left && lastX < mDotRect.right && lastY > mDotRect.top && lastY < mDotRect.bottom) {
                    state = TOUCH_PROCESS;
                } else {
                    state = TOUCH_DOWN;
                }
                Log.i("点击位置：", "x:" + lastX + ",y:" + lastY + "-----------------------");

                break;
            case MotionEvent.ACTION_UP:

                if (state == TOUCH_MOVE || state == TOUCH_DOWN) {
                    float x = event.getX() - lastX;
                    float y = event.getY() - lastY;
                    dispatchEvent(x, y);
                }
                backCenter();
                slideDirect = 0;
                state = TOUCH_NORMAL;
                break;
            case MotionEvent.ACTION_MOVE:

                if (state == TOUCH_DOWN || state == TOUCH_MOVE) {
                    distanceX = event.getRawX() - slideX;
                    distanceY = event.getRawY() - slideY;
                    if(slideDirect==0){
                        if(Math.abs(distanceX)>Math.abs(distanceY)){
                            slideDirect=1;
                        }
                        else{
                            slideDirect=2;
                        }
                    }
                    distanceX /= 2;
                    distanceY /= 2;

                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(layoutParams.width, layoutParams.height);
                    if (Math.abs(distanceX) > layoutParams.leftMargin) {
                        Log.i("水平滑动", "distanceX:" + distanceX);
                        distanceX = (distanceX > 0) ? layoutParams.leftMargin : (-layoutParams.leftMargin);
                    }
                    if(slideDirect==1){
                        params.leftMargin = (int) distanceX + layoutParams.leftMargin;
                        params.rightMargin = -(int) distanceX + layoutParams.leftMargin;

                        params.topMargin = layoutParams.topMargin;
                        params.bottomMargin =layoutParams.topMargin;
                    }

                    if (Math.abs(distanceY) > layoutParams.topMargin) {
                        Log.i("垂直滑动", "distanceY:" + distanceY);
                        distanceY = (distanceY > 0) ? layoutParams.topMargin : (-layoutParams.topMargin);
                    }
                    if(slideDirect==2){
                        params.leftMargin = layoutParams.leftMargin;
                        params.rightMargin = layoutParams.leftMargin;
                        params.topMargin = (int) distanceY + layoutParams.topMargin;
                        params.bottomMargin = -(int) distanceY + layoutParams.topMargin;
                    }
                    CircleImageView.this.setLayoutParams(params);
                }
                break;
        }
        return true;
    }

    private void backCenter() {
        this.post(new Runnable() {
            @Override
            public void run() {
                CircleImageView.this.setLayoutParams(layoutParams);
            }
        });

    }

    private void dispatchProcess(float x, float y) {
        float process = 0;
        if (x >= 0 && y >= 0) {
            process = 90 + (x == 0 ? 0 : (float) Math.asin((Math.abs(y) / Math.abs(x)))) * 180;
        } else if (x >= 0 && y < 0) {
            process = (y == 0 ? 0 : (float) Math.asin((Math.abs(x) / Math.abs(y)))) * 180;
        } else if (x < 0 && y >= 0) {
            process = 180 + (y == 0 ? 0 : (float) Math.asin((Math.abs(x) / Math.abs(y)))) * 180;
        } else {
            process = 270 + (x == 0 ? 0 : (float) Math.asin((Math.abs(y) / Math.abs(x)))) * 180;
        }
        mTouchSlideEvent.onProcess(process);
    }

    public void dispatchEvent(float x, float y) {
        Log.i("移动距离", "x:" + x + ",y:" + y);
        float r = (mDrawableRect.right - mDrawableRect.left) / 2;
        if (Math.abs(x) < (r / 2) && Math.abs(y) < (r / 2)) {
            //Log.i("事件", "点击");
            mTouchSlideEvent.onClick();
            return;
        }
        if (x >= 0 && y >= 0) {
            if (x > y) {
                //Log.i("事件", "右滑");
                mTouchSlideEvent.onRightSide();
            } else {
                //Log.i("事件", "下滑");
                mTouchSlideEvent.onBottomSide();
            }
        } else if (x >= 0 && y < 0) {
            if (Math.abs(x) > Math.abs(y)) {
                //Log.i("事件", "右滑");
                mTouchSlideEvent.onRightSide();
            } else {
                //Log.i("事件", "上滑");
                mTouchSlideEvent.onTopSide();
            }
        } else if (x < 0 && y >= 0) {
            if (Math.abs(x) > Math.abs(y)) {
                //Log.i("事件", "左滑");
                mTouchSlideEvent.onLeftSide();
            } else {
                //Log.i("事件", "下滑");
                mTouchSlideEvent.onBottomSide();
            }
        } else {
            if (Math.abs(x) > Math.abs(y)) {
                //Log.i("事件", "左滑");
                mTouchSlideEvent.onLeftSide();
            } else {
                //Log.i("事件", "上滑");
                mTouchSlideEvent.onTopSide();
            }
        }
    }

    public interface TouchSlideEvent {
        void onClick();

        void onLeftSide();

        void onTopSide();

        void onRightSide();

        void onBottomSide();

        void onProcess(float process);

        void onSlide(float distanceX, float distanceY);
    }

}

