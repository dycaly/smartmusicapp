package com.ktboys.dycaly.smartmusic.dataitem;

/**
 * Created by dycaly on 2016/11/18.
 */

public class MoodItem {
    private int moodImage;
    private String title;

    public MoodItem(int moodImage, String title) {
        this.moodImage = moodImage;
        this.title = title;
    }

    public int getMoodImage() {
        return moodImage;
    }

    public void setMoodImage(int moodImage) {
        this.moodImage = moodImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
