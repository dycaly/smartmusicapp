package com.ktboys.dycaly.smartmusic.activity;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ktboys.dycaly.smartmusic.R;
import com.ktboys.dycaly.smartmusic.adapter.MenuAdapter;
import com.ktboys.dycaly.smartmusic.adapter.MoodAdapter;
import com.ktboys.dycaly.smartmusic.dataitem.MenuItem;
import com.ktboys.dycaly.smartmusic.dataitem.MoodItem;
import com.ktboys.dycaly.smartmusic.dataitem.SongItem;
import com.ktboys.dycaly.smartmusic.fragment.AccountFragment;
import com.ktboys.dycaly.smartmusic.fragment.BrowseFragment;
import com.ktboys.dycaly.smartmusic.fragment.LibraryFragment;
import com.ktboys.dycaly.smartmusic.fragment.SearchFragment;
import com.ktboys.dycaly.smartmusic.fragment.SettingFragment;
import com.ktboys.dycaly.smartmusic.fragment.SocialFragment;
import com.ktboys.dycaly.smartmusic.manager.SongsManager;
import com.ktboys.dycaly.smartmusic.service.MusicService;
import com.ktboys.dycaly.smartmusic.utils.AudioInfo;
import com.ktboys.dycaly.smartmusic.utils.RadomMood;
import com.ktboys.dycaly.smartmusic.utils.ScanMusic;
import com.ktboys.dycaly.smartmusic.utils.SetBlurBg;
import com.ktboys.dycaly.smartmusic.view.CircleImageView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private float process = 0;
    private static ServiceToActivity serviceToActivity;

    private CircleImageView mPlayer;
    private de.hdodenhof.circleimageview.CircleImageView mStartPlayer;
    private RelativeLayout rootLayout;
    private DrawerLayout mDrawerLayout;
    private TextView nameText;
    private TextView artistText;
    private ListView menuListView;
    private MenuAdapter menuAdapter;
    private FrameLayout mFrameLayout;
    private TextView titleText;
    private ImageView maskView;
    private ImageView moreImage;
    private ImageView moodImage;
    private Bitmap defaultBgBitmap;
    private ImageView leftMenu;
    private Intent mIntent;
    private MusicService.MusicControler mMusicControler;
    private MyServiceConnection myServiceConnection;

    private static final int RESET = 0;
    private static final int START = 1;
    private static final int PAUSE = 2;
    private static int PlayerState = RESET;

    private ArrayList<Fragment> mFragments;
    private BrowseFragment browseFragment;
    private SearchFragment searchFragment;
    private SocialFragment socialFragment;
    private LibraryFragment libraryFragment;
    private SettingFragment settingFragment;
    private AccountFragment accountFragment;
    private FragmentManager fragmentManager;
    private int currentFragmentIndex = 0;
    private Drawable bgDrawable = null;

    private LinearLayout accountLayout;

    //弹出框PopWindow
    private PopupWindow mPopWindow;
    private View mPopView;
    private GridView moodGridView;
    private MoodAdapter moodAdapter;
    private LinearLayout likeshareLayout;
    private LinearLayout likeLayout;
    private ImageView likeImage;
    private LinearLayout shareLayout;
    private LinearLayout moodLayout;
    private LinearLayout sharetoLayout;
    private LinearLayout moreLayout;
    private TextView moreAlbumText;
    private TextView moreArtistText;
    private RelativeLayout slideAreaLayout;
    private static final int MODEL_MOOD = 0;
    private static final int MODEL_LIKE_SHARE = 1;
    private static final int MODEL_SHARE_TO = 2;
    private static final int MODEL_MORE = 3;

    private SongsManager mSongsManager;
    private SongItem mSongItem;
    private String mMood = "";

    private ArrayList<MoodItem> moodItems;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setOuterLayoutFullScreen();
        initView();
        setDefaultBgSongImg();
        initEvent();
        startMusicService();
        ScanMusic.scan(MainActivity.this);
        mSongsManager = new SongsManager(MainActivity.this);
        moodItems = new ArrayList<>();
        moodItems.add(new MoodItem(R.drawable.ic_happy, "Energetic"));
        moodItems.add(new MoodItem(R.drawable.ic_mood_romantic, "Romantic"));
        moodItems.add(new MoodItem(R.drawable.ic_mood_bright, "Bright"));
        moodItems.add(new MoodItem(R.drawable.ic_mood_relaxing, "Relaxing"));
        moodItems.add(new MoodItem(R.drawable.ic_mood_gloomy, "Gloomy"));
        moodItems.add(new MoodItem(R.drawable.ic_mood_angry, "Angry"));
        moodItems.add(new MoodItem(R.drawable.ic_mood_redefine, "Redefine"));
        randomMood();
    }


    private void randomMood(){
        mMood = RadomMood.getRadomMood();
        for(MoodItem item:moodItems){
            if(item.getTitle().toLowerCase().equals(mMood)){
                moodImage.setImageResource(item.getMoodImage());
                break;
            }
        }
    }


    /**
     * 设置默认背景及歌曲图片
     */
    private void setDefaultBgSongImg() {

        mStartPlayer.setVisibility(View.VISIBLE);
        moodImage.setVisibility(View.GONE);
        slideAreaLayout.setVisibility(View.GONE);
        nameText.setVisibility(View.GONE);
        artistText.setVisibility(View.GONE);
        SetBlurBg.set(MainActivity.this, BitmapFactory.decodeResource(getResources(), R.drawable.ic_background_regular), rootLayout);
    }

    private void playAnimation(View view) {


        int duration = 500;
        int dely = 500;
        AnimationSet as = new AnimationSet(false);
        TranslateAnimation ta = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0,
                Animation.RELATIVE_TO_PARENT, 0,
                Animation.RELATIVE_TO_PARENT, 0,
                Animation.RELATIVE_TO_PARENT, 3f);
        ta.setDuration(duration);
        ta.setStartOffset(dely);
        ta.setFillAfter(true);
        as.addAnimation(ta);

        ScaleAnimation sa = new ScaleAnimation(1.0f, 0.19f, 1.0f, 0.19f, view.getWidth() / 2, view.getHeight() / 2);
        sa.setDuration(duration);
        sa.setFillAfter(true);
        as.addAnimation(sa);

        AlphaAnimation aa = new AlphaAnimation(1.0f, 0f);
        aa.setDuration(duration);
        aa.setFillAfter(true);
        aa.setStartOffset(dely);
        as.addAnimation(aa);
        as.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                playFirst();
                moodImage.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(as);

    }

    /**
     * 播放音乐
     *
     * @param songItem
     */
    public void playMusic(SongItem songItem) {
        setMediaInfo(songItem);
        mMusicControler.playMusic(songItem.getFilePath());
    }

    /**
     * 设置当前播放的音乐的信息
     *
     * @param songItem
     */
    private void setMediaInfo(SongItem songItem) {
        mStartPlayer.setVisibility(View.GONE);
        slideAreaLayout.setVisibility(View.VISIBLE);
        moodImage.setVisibility(View.VISIBLE);
        nameText.setVisibility(View.VISIBLE);
        artistText.setVisibility(View.VISIBLE);
        nameText.setText(songItem.getTitle());
        artistText.setText(songItem.getArtist());
        Bitmap bitmap = AudioInfo.getAlbumArt(songItem.getFilePath());
        if (bitmap == null) {
            Log.i("专辑图", "空");
            SetBlurBg.set(MainActivity.this, BitmapFactory.decodeResource(getResources(), R.drawable.ic_background_regular), rootLayout);
            mPlayer.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_background_regular));
        } else {
            Log.i("专辑图", "非空");
            SetBlurBg.set(MainActivity.this, bitmap, rootLayout);
            mPlayer.setImageBitmap(bitmap);
        }
    }

    /**
     * 播放第一首歌
     */
    private void playFirst() {
        PlayerState = RESET;
        mPlayer.setProcess(0);
        mSongItem = mSongsManager.getFirstSong(mMood);
        if (mSongItem != null) {
            playMusic(mSongItem);
            PlayerState = START;
        } else {
            setDefaultBgSongImg();
            Toast.makeText(MainActivity.this, mMood + "心情音乐为空！", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * 播放上一首歌
     */
    private void playLast() {
        PlayerState = RESET;
        mPlayer.setProcess(0);
        if (mSongItem != null) {
            mSongItem = mSongsManager.getLastSong(mMood, mSongItem.getId());
            if (mSongItem != null) {
                playMusic(mSongItem);
                PlayerState = START;
            } else {
                setDefaultBgSongImg();
                Toast.makeText(MainActivity.this, mMood + "心情音乐为空！", Toast.LENGTH_SHORT).show();
            }
        } else {
            playFirst();
        }
    }

    /**
     * 播放下一首歌
     */
    private void playNext() {
        PlayerState = RESET;
        mPlayer.setProcess(0);
        if (mSongItem != null) {
            mSongItem = mSongsManager.getNextSong(mMood, mSongItem.getId());
            if (mSongItem != null) {
                playMusic(mSongItem);
                PlayerState = START;
            } else {
                setDefaultBgSongImg();
                Toast.makeText(MainActivity.this, mMood + "心情音乐为空！", Toast.LENGTH_SHORT).show();
            }
        } else {
            playFirst();
        }
    }

    /**
     * 开启音乐服务
     */
    private void startMusicService() {
        mIntent = new Intent(MainActivity.this, MusicService.class);
        startService(mIntent);
        myServiceConnection = new MyServiceConnection();
        bindService(mIntent, myServiceConnection, BIND_AUTO_CREATE);
    }

    /**
     * 初始化控件
     */
    private void initView() {
        rootLayout = (RelativeLayout) findViewById(R.id.activity_main);
        maskView = (ImageView) findViewById(R.id.id_mask);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.id_drawer);
        mPlayer = (CircleImageView) findViewById(R.id.id_player);
        mStartPlayer = (de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.id_start_player);
        slideAreaLayout = (RelativeLayout) findViewById(R.id.id_slide_area);
        nameText = (TextView) findViewById(R.id.id_audio_name);
        artistText = (TextView) findViewById(R.id.id_audio_artist);
        titleText = (TextView) findViewById(R.id.id_main_title);
        moreImage = (ImageView) findViewById(R.id.id_more_iv);
        moodImage = (ImageView) findViewById(R.id.id_mood_iv);
        menuListView = (ListView) findViewById(R.id.id_menu_lv);
        leftMenu = (ImageView) findViewById(R.id.id_left_menu);
        accountLayout = (LinearLayout) findViewById(R.id.id_account_ll);
        menuAdapter = new MenuAdapter(MainActivity.this);
        menuListView.setAdapter(menuAdapter);
        menuAdapter.addItem(new MenuItem("Smart FM"));
        menuAdapter.addItem(new MenuItem("Browse"));
        menuAdapter.addItem(new MenuItem("Search"));
        menuAdapter.addItem(new MenuItem("Social"));
        menuAdapter.addItem(new MenuItem("My Library"));
        menuAdapter.addItem(new MenuItem("Setting"));
        menuAdapter.selectItem(0);
        mFrameLayout = (FrameLayout) findViewById(R.id.id_frame);
        browseFragment = new BrowseFragment();
        searchFragment = new SearchFragment();
        socialFragment = new SocialFragment();
        libraryFragment = new LibraryFragment();
        settingFragment = new SettingFragment();
        accountFragment = new AccountFragment();
        mFragments = new ArrayList<>();
        mFragments.add(browseFragment);
        mFragments.add(searchFragment);
        mFragments.add(socialFragment);
        mFragments.add(libraryFragment);
        mFragments.add(settingFragment);
        mFragments.add(accountFragment);
        fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.id_frame, browseFragment);
        transaction.add(R.id.id_frame, searchFragment);
        transaction.add(R.id.id_frame, socialFragment);
        transaction.add(R.id.id_frame, libraryFragment);
        transaction.add(R.id.id_frame, settingFragment);
        transaction.add(R.id.id_frame, accountFragment);
        transaction.hide(browseFragment);
        transaction.hide(searchFragment);
        transaction.hide(socialFragment);
        transaction.hide(libraryFragment);
        transaction.hide(settingFragment);
        transaction.hide(accountFragment);
        transaction.commit();
    }

    /**
     * 初始化事件
     */
    private void initEvent() {
        //菜单项点击事件
        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                menuAdapter.selectItem(i);
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                titleText.setText(menuAdapter.getItem(i).getTitle());
                if (i == 0) {
                    mFrameLayout.setVisibility(View.GONE);
                    rootLayout.setBackground(bgDrawable);
                    bgDrawable = null;
                } else {
                    Log.i("切换", "------" + i + "----------------");
                    if (bgDrawable == null) {
                        bgDrawable = rootLayout.getBackground();
                    }
                    rootLayout.setBackgroundResource(R.drawable.ic_background_regular);
                    mFrameLayout.setVisibility(View.VISIBLE);
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    if (currentFragmentIndex != 0) {
                        transaction.hide(mFragments.get(currentFragmentIndex - 1));
                    }
                    transaction.show(mFragments.get(i - 1));
                    transaction.commit();
                    currentFragmentIndex = i;
                }


            }
        });
        mStartPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playAnimation(mStartPlayer);
            }
        });

        //菜单弹出事件
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        findViewById(R.id.id_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
                ((SearchFragment)mFragments.get(1)).close();
            }
        });

        leftMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }
        });

        //播放器出手势事件
        mPlayer.setTouchSlideEvent(new CircleImageView.TouchSlideEvent() {
            @Override
            public void onClick() {

                switch (PlayerState) {
                    case RESET:
                        playFirst();
                        break;
                    case START:
                        mMusicControler.pauseMusic();
                        PlayerState = PAUSE;
                        break;
                    case PAUSE:
                        mMusicControler.continueMusic();
                        PlayerState = START;
                        break;
                }

            }

            @Override
            public void onLeftSide() {
                playLast();
            }

            @Override
            public void onTopSide() {

            }

            @Override
            public void onRightSide() {
                playNext();
            }

            @Override
            public void onBottomSide() {
                popMainView(false, MODEL_LIKE_SHARE);
            }

            @Override
            public void onProcess(float process) {
                Log.i("进度", process + "");
            }

            @Override
            public void onSlide(float distanceX, float distanceY) {

            }
        });

        serviceToActivity = new ServiceToActivity() {
            @Override
            public void playComplete() {
                mPlayer.setProcess(0);
                PlayerState = RESET;
                playNext();
            }

            @Override
            public void playProcess(double process) {
                mPlayer.setProcess(process);
            }

            @Override
            public void playError() {
                mPlayer.setProcess(0);
                PlayerState = RESET;
            }
        };

        moodImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popMainView(true, MODEL_MOOD);
            }
        });

        moreImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popMainView(true, MODEL_MORE);
            }
        });

        accountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuAdapter.selectNothing();
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                titleText.setText("Account");
                int i = 6;
                Log.i("切换", "------" + i + "----------------");
                if (bgDrawable == null) {
                    bgDrawable = rootLayout.getBackground();
                }
                rootLayout.setBackgroundResource(R.drawable.ic_account_toped);
                mFrameLayout.setVisibility(View.VISIBLE);
                FragmentTransaction transaction = fragmentManager.beginTransaction();

                if (currentFragmentIndex != 0) {
                    transaction.hide(mFragments.get(currentFragmentIndex - 1));
                }

                transaction.show(mFragments.get(i - 1));
                transaction.commit();
                currentFragmentIndex = i;
            }
        });
    }


    /**
     * 弹出PopWindows
     *
     * @param isBottom
     * @param flag
     */
    private void popMainView(boolean isBottom, int flag) {
        if (mPopView == null) {
            mPopView = LayoutInflater.from(MainActivity.this).inflate(R.layout.popwindow_main, null);
            likeshareLayout = (LinearLayout) mPopView.findViewById(R.id.id_like_share_ll);
            likeLayout = (LinearLayout) mPopView.findViewById(R.id.id_ls_like);
            likeImage = (ImageView) mPopView.findViewById(R.id.id_ls_like_iv);
            shareLayout = (LinearLayout) mPopView.findViewById(R.id.id_ls_share);
            likeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mSongItem != null) {
                        mSongItem.setLike(!mSongItem.isLike());
                        if (mSongsManager.setLiske(mSongItem.getId(), mSongItem.isLike())) {
                            likeImage.setImageResource(mSongItem.isLike() ? R.drawable.ic_liked : R.drawable.ic_like);
                        }
                    }
                }
            });
            shareLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    moodLayout.setVisibility(View.GONE);
                    likeshareLayout.setVisibility(View.GONE);
                    moreLayout.setVisibility(View.GONE);
                    sharetoLayout.setVisibility(View.VISIBLE);
                }
            });

            moodLayout = (LinearLayout) mPopView.findViewById(R.id.id_mood_ll);
            sharetoLayout = (LinearLayout) mPopView.findViewById(R.id.id_share_to_ll);
            moreLayout = (LinearLayout) mPopView.findViewById(R.id.id_more_ll);
            moreAlbumText = (TextView) mPopView.findViewById(R.id.id_more_album_tv);
            moreArtistText = (TextView) mPopView.findViewById(R.id.id_more_artist_tv);
            moodGridView = (GridView) mPopView.findViewById(R.id.id_mood_gv);
            moodAdapter = new MoodAdapter(MainActivity.this);
            moodGridView.setAdapter(moodAdapter);
            moodGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    moodImage.setVisibility(View.VISIBLE);
                    mMusicControler.stopMusic();
                    MoodItem moodItem = moodAdapter.getItem(i);
                    if(moodItem.getTitle().toLowerCase().equals("redefine")){
                        randomMood();
                    }else{
                        mMood = moodItem.getTitle().toLowerCase();
                        moodImage.setImageResource(moodItem.getMoodImage());
                    }

                    if (mPopWindow != null && mPopWindow.isShowing()) {
                        mPopWindow.dismiss();
                    }
                    playFirst();
                    moodImage.setVisibility(View.VISIBLE);

                }
            });
        }

        switch (flag) {
            case MODEL_MOOD:
                sharetoLayout.setVisibility(View.GONE);
                likeshareLayout.setVisibility(View.GONE);
                moreLayout.setVisibility(View.GONE);
                moodLayout.setVisibility(View.VISIBLE);
                moodAdapter.clear();
                for (MoodItem moodItem : moodItems) {
                    if (moodItem.getTitle().toLowerCase().equals(mMood)) {
                        continue;
                    }
                    moodAdapter.addItem(moodItem);
                }
                break;
            case MODEL_LIKE_SHARE:
                moodLayout.setVisibility(View.GONE);
                sharetoLayout.setVisibility(View.GONE);
                moreLayout.setVisibility(View.GONE);
                likeImage.setImageResource((mSongItem != null && mSongItem.isLike()) ? R.drawable.ic_liked : R.drawable.ic_like);
                likeshareLayout.setVisibility(View.VISIBLE);
                break;
            case MODEL_SHARE_TO:
                moodLayout.setVisibility(View.GONE);
                likeshareLayout.setVisibility(View.GONE);
                moreLayout.setVisibility(View.GONE);
                sharetoLayout.setVisibility(View.VISIBLE);
                break;
            case MODEL_MORE:
                moodLayout.setVisibility(View.GONE);
                likeshareLayout.setVisibility(View.GONE);
                sharetoLayout.setVisibility(View.GONE);
                if (mSongItem != null) {
                    moreAlbumText.setText(mSongItem.getAlbum());
                    moreArtistText.setText(mSongItem.getArtist());
                }
                moreLayout.setVisibility(View.VISIBLE);
                break;
        }

        mPopWindow = new PopupWindow(mPopView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mPopWindow.setOutsideTouchable(true);
        mPopWindow.setFocusable(true);
        if (isBottom) {
            mPopWindow.setAnimationStyle(R.style.AnimBottom);
        }
        happenAnim();
        mPopWindow.showAtLocation(findViewById(R.id.id_mood_iv), Gravity.TOP, 0, 0);
        mPopView.setFocusable(true); // 这个很重要
        mPopView.setFocusableInTouchMode(true);
        //设置消失动画
        mPopWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                disapearAnim();
                mPopWindow = null;
            }
        });
        //点击其他地方消失
        mPopView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (mPopWindow != null && mPopWindow.isShowing()) {
                    mPopWindow.dismiss();
                }
                return false;
            }
        });
    }


    /**
     * 半透明背景出现的动画
     */
    private void happenAnim() {
        ObjectAnimator animator = ObjectAnimator.ofFloat(maskView, "alpha", 0, 0.2f, 0.5f, 0.7f, 0.9f, 1);
        animator.setDuration(300);
        animator.setRepeatCount(0);
        animator.start();
        maskView.setVisibility(View.VISIBLE);
    }

    /**
     * 半透明背景消失的动画
     */
    public void disapearAnim() {
        ObjectAnimator animator = ObjectAnimator.ofFloat(maskView, "alpha", 1, 0.9f, 0.7f, 0.5f, 0.2f, 0);
        animator.setDuration(300);
        animator.setRepeatCount(0);
        animator.start();
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                maskView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }


    /**
     * 设置全屏背景
     */
    public void setOuterLayoutFullScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            ViewGroup rootView = (ViewGroup) ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
            rootView.setFitsSystemWindows(true);
            rootView.setClipToPadding(true);
        }
    }

    /**
     * Activity销毁
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(myServiceConnection);
        stopService(mIntent);
        mSongsManager.close();
    }

    public static Handler processHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle data = msg.getData();
            double process = data.getDouble("process");
            serviceToActivity.playProcess(process);
        }
    };
    public static Handler compliteHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            serviceToActivity.playComplete();

        }
    };
    public static Handler errorHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            serviceToActivity.playError();
        }
    };

    class MyServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mMusicControler = (MusicService.MusicControler) iBinder;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    }

    private interface ServiceToActivity {
        void playComplete();

        void playProcess(double process);

        void playError();
    }
}
