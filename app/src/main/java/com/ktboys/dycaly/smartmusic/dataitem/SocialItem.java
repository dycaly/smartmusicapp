package com.ktboys.dycaly.smartmusic.dataitem;

/**
 * Created by dycaly on 2016/11/22.
 */

public class SocialItem {
    private int headImage;
    private String name;
    private int sex;
    private String distance;
    private int moodImage;
    private String music;

    public SocialItem(int headImage, String name, int sex, String distance, int moodImage, String music) {
        this.headImage = headImage;
        this.name = name;
        this.sex = sex;
        this.distance = distance;
        this.moodImage = moodImage;
        this.music = music;
    }

    public int getHeadImage() {
        return headImage;
    }

    public void setHeadImage(int headImage) {
        this.headImage = headImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getMoodImage() {
        return moodImage;
    }

    public void setMoodImage(int moodImage) {
        this.moodImage = moodImage;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }
}
