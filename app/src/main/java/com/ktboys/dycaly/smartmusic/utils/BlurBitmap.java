package com.ktboys.dycaly.smartmusic.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;

/**
 * Created by dycaly on 2016/11/15.
 */

public class BlurBitmap {
    /**
     * 图片缩放比例
     */
    private static final float BITMAP_SCALE = 0.8f;
    /**
     * 最大模糊度(在0.0到25.0之间)
     */
    private static final float BLUR_RADIUS = 20f;
    /**
     * 图片最大宽度
     */
    private static final int MAX_WIDTH = 540;

    /**
     * 模糊图片的具体方法
     *
     * @param context 上下文对象
     * @param image   需要模糊的图片
     * @return 模糊处理后的图片
     */
    public static Bitmap blur(Context context, Bitmap image, int screenWidth, int screenHeight) {

        //剪裁图片
        float screenRatio = (float) screenHeight / (float) screenWidth;
        // 计算图片缩小后的长宽
        int width = image.getWidth();
        int height = image.getHeight();
        Log.i("图像宽高", "width:" + width + ",height:" + height);
        float ratio = (float) height / (float) width;
        int startX = 0;
        int startY = 0;
        if (screenRatio > ratio) {
            int w = Math.round(height / screenRatio);
            startX = (width - w) / 2;
            width = w;
        } else {
            int h = Math.round(width * screenRatio);
            startY = (height - h) / 2;
            height = h;
        }
        Log.i("剪切起点", "startX:" + startX + ",startY:" + startY);
        Log.i("剪切宽高", "width:" + width + ",height:" + height);
        Bitmap srcBitmap = Bitmap.createBitmap(image, startX, startY, width, height);

        // 缩小图片
        while (width > MAX_WIDTH) {
            width = Math.round(width * BITMAP_SCALE);
            height = Math.round(height * BITMAP_SCALE);
        }
        Bitmap inputBitmap = Bitmap.createScaledBitmap(srcBitmap, width, height, false);
        // 创建一张渲染后的输出图片。
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);
        // 创建RenderScript内核对象
        RenderScript rs = RenderScript.create(context);
        // 创建一个模糊效果的RenderScript的工具对象
        ScriptIntrinsicBlur blurScript = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        // 由于RenderScript并没有使用VM来分配内存,所以需要使用Allocation类来创建和分配内存空间。
        // 创建Allocation对象的时候其实内存是空的,需要使用copyTo()将数据填充进去。
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);

        // 设置渲染的模糊程度, 25f是最大模糊度
        blurScript.setRadius(BLUR_RADIUS);
        // 设置blurScript对象的输入内存
        blurScript.setInput(tmpIn);
        // 将输出数据保存到输出内存中
        blurScript.forEach(tmpOut);

        // 将数据填充到Allocation中
        tmpOut.copyTo(outputBitmap);

        //释放
        tmpIn.destroy();
        tmpOut.destroy();
        rs.destroy();
        srcBitmap.recycle();
        inputBitmap.recycle();

        return outputBitmap;
    }

}
