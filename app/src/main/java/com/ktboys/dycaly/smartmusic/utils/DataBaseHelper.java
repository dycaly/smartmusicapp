package com.ktboys.dycaly.smartmusic.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by dycaly on 2015/12/18.
 */
public class DataBaseHelper extends SQLiteOpenHelper{

    private static DataBaseHelper dataBaseHelper = null;
    private static Context mContext;
    private DataBaseHelper(Context context) {
        super(context, "smartmusic.db", null, 1);
    }
    public static DataBaseHelper getInstance(Context context){
        mContext = context;
        if (dataBaseHelper == null){
            dataBaseHelper = new DataBaseHelper(context);
        }
        return dataBaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("数据库提示","创建数据库");
        try{
            Log.i("数据库提示","创建表song--开始");
            db.execSQL("create table song(_id integer primary key AUTOINCREMENT," +
                    "filepath varchar(255), " +
                    "title varchar(255), " +
                    "album varchar(255), " +
                    "artist varchar(255), " +
                    "mood varchar(255), " +
                    "like integer(10),"+
                    "skip integer(10))");
            Log.i("数据库提示", "创建表song--成功");
        }
        catch (Exception e){
            Log.e("数据库提示","创建表错误："+e.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
