package com.ktboys.dycaly.smartmusic.dataitem;

/**
 * Created by dycaly on 2016/11/18.
 */

public class MenuItem {
    private String title;
    private boolean selected;

    public MenuItem(String title) {
        this.title = title;
        this.selected = false;
    }

    public String getTitle() {
        return title;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
