package com.ktboys.dycaly.smartmusic.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.ktboys.dycaly.smartmusic.R;
import com.ktboys.dycaly.smartmusic.adapter.LibMusicAdapter;
import com.ktboys.dycaly.smartmusic.adapter.LibTopAdapter;
import com.ktboys.dycaly.smartmusic.dataitem.LibMusicItem;
import com.ktboys.dycaly.smartmusic.dataitem.LibTopItem;

/**
 * Created by dycaly on 2016/11/18.
 */

public class LibraryFragment extends Fragment {

    private View rootView;
    private ListView topListView;
    private LibTopAdapter libTopAdapter;
    private ListView musicListView;
    private LibMusicAdapter libMusicAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_library, container, false);
        initView();
        initEvent();
        return rootView;
    }

    private void initView() {
        topListView = (ListView) rootView.findViewById(R.id.id_lib_top_lv);
        libTopAdapter = new LibTopAdapter(getActivity());
        topListView.setAdapter(libTopAdapter);
        libTopAdapter.addItem(new LibTopItem(R.drawable.ic_songs,"Songs",56));
        libTopAdapter.addItem(new LibTopItem(R.drawable.ic_artist,"Artists",16));
        libTopAdapter.addItem(new LibTopItem(R.drawable.ic_album,"Albums",5));
        libTopAdapter.addItem(new LibTopItem(R.drawable.ic_playlist,"Playlists",156));
        libTopAdapter.addItem(new LibTopItem(R.drawable.ic_offline,"Offine",6));
        setListViewHeight(libTopAdapter,topListView);

        musicListView = (ListView) rootView.findViewById(R.id.id_lib_bottom_lv);
        libMusicAdapter = new LibMusicAdapter(getActivity());
        musicListView.setAdapter(libMusicAdapter);
        libMusicAdapter.addItem(new LibMusicItem(R.drawable.ic_backinblack,"Black in Black","AC DC"));
        libMusicAdapter.addItem(new LibMusicItem(R.drawable.ic_vercaseonthefloor,"Vercase on the floor","Bruno Mars"));
        libMusicAdapter.addItem(new LibMusicItem(R.drawable.ic_newreleasealbum,"Hardware...to Self-destruct","Metallica"));
        setListViewHeight(libMusicAdapter,musicListView);
    }

    private void initEvent() {
    }

    public void setListViewHeight(BaseAdapter adapter, ListView listView) {
        // 获取ListView对应的Adapter
        if (adapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0, len = adapter.getCount(); i < len; i++) { // listAdapter.getCount()返回数据项的数目
            View listItem = adapter.getView(i, null, listView);
            listItem.measure(0, 0); // 计算子项View 的宽高
            totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount()));
        listView.setLayoutParams(params);
    }
}
