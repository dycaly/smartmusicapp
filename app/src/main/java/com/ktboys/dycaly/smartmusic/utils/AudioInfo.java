package com.ktboys.dycaly.smartmusic.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.provider.MediaStore;
import android.util.Log;

import java.util.HashMap;


/**
 * Created by dycaly on 2016/11/17.
 */

public class AudioInfo {

    public static HashMap<String,String> getAudioInfo(Context context, String filePath) {
        HashMap<String,String> info = null;

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null,
                MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
        String path = null;
        if (cursor.moveToFirst()) {
            do {
                // 通过Cursor 获取路径，如果路径相同则break；
                path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA));
                if (path.equals(filePath)) {
                    String tilte = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE));
                    String album = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM));
                    String artist = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST));
                    info = new HashMap<>();
                    info.put("title",tilte);
                    info.put("album",album);
                    info.put("artist",artist);
                    break;
                }
            } while (cursor.moveToNext());
        }

        if(info == null){
            info = new HashMap<>();
            String title = "未知歌曲名";
            try {
                String[] strings = filePath.split("/");
                title = strings[strings.length-1].split(".")[0];
            }
            catch (Exception e){

            }
            info.put("title",title);
            info.put("album","未知");
            info.put("artist","未知艺术家");
        }
        return info;
    }

    public static Bitmap getAlbumArt(String filePath) {
        Bitmap bitmap = null;
        //能够获取多媒体文件元数据的类
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(filePath); //设置数据源
            byte[] embedPic = retriever.getEmbeddedPicture(); //得到字节型数据
            bitmap = BitmapFactory.decodeByteArray(embedPic, 0, embedPic.length); //转换为图片
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return bitmap;
    }

}
