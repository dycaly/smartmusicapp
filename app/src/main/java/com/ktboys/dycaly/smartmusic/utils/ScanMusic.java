package com.ktboys.dycaly.smartmusic.utils;

import android.content.Context;
import android.util.Log;

import com.ktboys.dycaly.smartmusic.activity.MainActivity;
import com.ktboys.dycaly.smartmusic.config.MainConfig;
import com.ktboys.dycaly.smartmusic.dataitem.SongItem;
import com.ktboys.dycaly.smartmusic.manager.SongsManager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dycaly on 2016/11/19.
 */

public class ScanMusic {
    public static void scan(Context context){
        SongsManager sm = new SongsManager(context);
        sm.clearSongs();
        ArrayList<String> moodStrs = new ArrayList<>();
        moodStrs.add("energetic");
        moodStrs.add("bright");
        moodStrs.add("angry");
        moodStrs.add("gloomy");
        moodStrs.add("relaxing");
        moodStrs.add("romantic");
        for(String str : moodStrs){
            File dirFolder = new File(MainConfig.MUSIC_PATH + "/"+str);
            if (!dirFolder.exists()) {
                dirFolder.mkdirs();//创建文件夹

            }
            else if(dirFolder.listFiles() != null){
                for(File file:dirFolder.listFiles()){
                    String filePath = file.getAbsolutePath();
                    HashMap<String, String> info = AudioInfo.getAudioInfo(context, filePath);
                    SongItem songItem = new SongItem(0,filePath,info.get("title"),info.get("album"),info.get("artist"),str,false,false);
                    sm.addSong(songItem);
                }
            }
        }
        sm.close();
    }
}
