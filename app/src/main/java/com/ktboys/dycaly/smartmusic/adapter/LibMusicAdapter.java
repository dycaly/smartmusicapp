package com.ktboys.dycaly.smartmusic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ktboys.dycaly.smartmusic.R;
import com.ktboys.dycaly.smartmusic.dataitem.LibMusicItem;
import com.ktboys.dycaly.smartmusic.dataitem.LibTopItem;

import java.util.ArrayList;

/**
 * Created by dycaly on 2016/11/18.
 */

public class LibMusicAdapter extends BaseAdapter {
    private ArrayList<LibMusicItem> datas;
    private Context context;
    public LibMusicAdapter(Context context){
        this.context = context;
        datas = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return datas.size();
    }
    public void clear(){
        datas.clear();
        notifyDataSetChanged();
    }
    public void addItem(LibMusicItem item){
        datas.add(item);
        notifyDataSetChanged();
    }
    @Override
    public LibMusicItem getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder = null;
        if (view == null){
            holder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.lv_library_music_item,null);
            holder.image = (ImageView) view.findViewById(R.id.id_llmi_image);
            holder.nameText = (TextView) view.findViewById(R.id.id_llmi_name);
            holder.artistText = (TextView) view.findViewById(R.id.id_llmi_artist);
            view.setTag(holder);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }
        LibMusicItem item = datas.get(i);
        holder.image.setImageResource(item.getImage());
        holder.nameText.setText(item.getName());
        holder.artistText.setText(item.getArtist());
        return view;
    }

    class ViewHolder{
        ImageView image;
        TextView nameText;
        TextView artistText;
    }
}
